//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace siente.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Concesion
    {
        public Concesion()
        {
            this.HistoriaCamions = new HashSet<HistoriaCamion>();
            this.Permisoes = new HashSet<Permiso>();
        }
    
        public System.Guid Id { get; set; }
        public int Ruta { get; set; }
        public Nullable<System.Guid> IdEmpresa { get; set; }
        public Nullable<System.Guid> IdCamion { get; set; }
        public Nullable<System.Guid> IdConcesionario { get; set; }
        public Nullable<System.Guid> IdBeneficiario { get; set; }
    
        public virtual Camion Camion { get; set; }
        public virtual Persona Persona { get; set; }
        public virtual Persona Persona1 { get; set; }
        public virtual ICollection<HistoriaCamion> HistoriaCamions { get; set; }
        public virtual ICollection<Permiso> Permisoes { get; set; }
    }
}
